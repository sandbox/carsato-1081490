  This is a simple module to show the load time of your site pages by uid.

  You enable the module and it starts recording load times for every single request. Then it makes a table with the load time data for each path and user, so you will have a table data showing the load time for a url of your site ordered by time, path or user id. 

  There is some nice alternatives like devel module. Devel module shows you lot of information about times and queries but load time module allows you to track your visitors and paths and see whats going on. You can see if lot of users have load problems or just one and which paths.

  It implements the boot and footer hooks. It takes timestamps at boot_hook and footer_hook, so it shows the time after drupal boots until it builds the footer. The module makes few and constant number of queries, so the module overload for your site is constant and doesn't increase the load time.

You can contact the autor trough drupal.org ( user: carsato ) or at carsato1 (at) yahoo.es

